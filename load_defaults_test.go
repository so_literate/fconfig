package fconfig_test

import (
	"errors"
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/so_literate/fconfig"
)

func TestLoadDefaults(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int     `default:"5"`
		String string  `default:"text"`
		Slice  []int64 `default:"1,2"`
		Struct struct {
			SField string `default:"SField"`
		}
		Empty int
	}
	got := config{}

	loader := fconfig.New(&got).SkipFiles().SkipEnv().SkipFlags()
	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		Int:    5,
		String: "text",
		Slice:  []int64{1, 2},
	}
	want.Struct.SField = "SField"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	type badConfig struct {
		Int int `default:"text"`
	}

	loader.Destination = &badConfig{}
	err = loader.Load()
	if !errors.Is(err, strconv.ErrSyntax) {
		t.Fatal(err)
	}
}
