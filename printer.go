package fconfig

import (
	"flag"
	"fmt"
	"io"
	"os"

	"gitlab.com/so_literate/fconfig/decoder/json"
	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/util"
)

func (l *Loader) getOutputWriter(output ...io.Writer) (out io.Writer) {
	switch {
	case len(output) != 0:
		out = output[0]
	case l.Config.Output != nil:
		out = l.Config.Output
	default:
		out = os.Stdout
	}

	return out
}

func (l *Loader) initPrinter() (dst interface{}, err error) {
	if err = l.init(); err != nil {
		return nil, fmt.Errorf("init: %w", err)
	}

	dst = util.AllocCopyValue(l.Destination)

	l.fields = analyzer.New(true, tags...).GetFields(dst)
	if len(l.fields) == 0 {
		return nil, fmt.Errorf("%w: no field to parse", ErrInvalidConfigDestination)
	}

	if err = l.loadDefaults(); err != nil {
		return nil, fmt.Errorf("failed to load defaults: %w", err)
	}

	return dst, nil
}

// PrintFlagDefaults prints help message from flag set with default values.
// You can set your output writer, by default printer will use loader.Config.Output if seted otherwise os.Stdout.
func (l *Loader) PrintFlagDefaults(output ...io.Writer) error {
	oldConfig := l.Config

	configCopy := *l.Config
	l.Config = &configCopy

	out := l.getOutputWriter(output...)

	if _, err := l.initPrinter(); err != nil {
		return fmt.Errorf("failed to init printer: %w", err)
	}

	if l.Config.Flags == nil {
		l.Config.Flags = flag.NewFlagSet(l.Config.PrefixFlag, flag.ContinueOnError)
	}

	if err := l.setFlags(); err != nil {
		return fmt.Errorf("failed to set flags: %w", err)
	}

	l.Config.Flags.SetOutput(out)
	l.Config.Flags.PrintDefaults()

	l.Config = oldConfig

	return nil
}

// PrintEnvDefaults prints environment variables with default values in format: NAME="VALUE".
// You can set your output writer, by default printer will use loader.Config.Output if seted otherwise os.Stdout.
func (l *Loader) PrintEnvDefaults(output ...io.Writer) error {
	out := l.getOutputWriter(output...)

	if _, err := l.initPrinter(); err != nil {
		return fmt.Errorf("failed to init printer: %w", err)
	}

	for _, field := range l.fields {
		defaultValue, _ := field.GetTag(TagDefault)
		fmt.Fprintf(out, "%s=%q\n", l.getEnvName(field), defaultValue)
	}

	return nil
}

// PrintEnvK8SDefaults prints environment variables with default values in format K8S values.yaml:
//     env:
//       - { name: NAME, value: "VALUE" }
// You can set your output writer, by default printer will use loader.Config.Output if seted otherwise os.Stdout.
func (l *Loader) PrintEnvK8SDefaults(output ...io.Writer) error {
	out := l.getOutputWriter(output...)

	if _, err := l.initPrinter(); err != nil {
		return fmt.Errorf("failed to init printer: %w", err)
	}

	for _, field := range l.fields {
		defaultValue, _ := field.GetTag(TagDefault)

		left := fmt.Sprintf("- { name: %s,", l.getEnvName(field))
		right := fmt.Sprintf("value: %q }", defaultValue)

		fmt.Fprintf(out, "%-50s%s\n", left, right)
	}

	return nil
}

// PrintFileDefaults prints default values into file decoder format. Be aware time.Duration is a int64 not a string.
// You can set your output writer, by default printer will use loader.Config.Output if seted otherwise os.Stdout.
func (l *Loader) PrintFileDefaults(decoder FileDecoder, output ...io.Writer) error {
	out := l.getOutputWriter(output...)

	dst, err := l.initPrinter()
	if err != nil {
		return fmt.Errorf("failed to init printer: %w", err)
	}

	res, err := decoder.Marshal(dst)
	if err != nil {
		return fmt.Errorf("failed to marshal config: %w", err)
	}

	fmt.Fprintf(out, "%s\n", res)
	return nil
}

// PrintFileJSON prints defuaut values into JSON file. Be aware time.Duration is a int64 not a string.
// You can set your output writer, by default printer will use loader.Config.Output if seted otherwise os.Stdout.
func (l *Loader) PrintFileJSON(output ...io.Writer) error {
	if err := l.PrintFileDefaults(json.Decoder, output...); err != nil {
		return fmt.Errorf("failed to PrintFileDefaults: %w", err)
	}

	return nil
}
