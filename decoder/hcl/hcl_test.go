package hcl_test

import (
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/decoder/hcl"
)

func TestFormats(t *testing.T) {
	t.Parallel()

	formats := hcl.Decoder.Formats()
	if !reflect.DeepEqual(formats, []string{".hcl"}) {
		t.Fatal(formats)
	}
}

type File struct {
	Int    int    `hcl:"int"`
	String string `hcl:"string"`
	Slice  []int  `hcl:"slice"`
	Struct struct {
		Field string `hcl:"field"`
	} `hcl:"struct,block"`
	Empty int `hcl:"empty,optional"`
}

func TestDecodeFile(t *testing.T) {
	t.Parallel()

	got := File{}

	err := hcl.Decoder.DecodeFile("./testdata/file.hcl", &got)
	if err != nil {
		t.Fatal(err)
	}

	want := File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	want.Struct.Field = "sub field"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// file not found
	err = hcl.Decoder.DecodeFile("./testdata/not_found.hcl", &got)
	if os.IsNotExist(err) {
		t.Fatal(err)
	}
}

func TestMarshal(t *testing.T) {
	t.Parallel()

	val := &File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	val.Struct.Field = "sub field"

	got, err := hcl.Decoder.Marshal(val)
	if err != nil {
		t.Fatal(err)
	}

	want, err := ioutil.ReadFile("./testdata/file.hcl")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(string(want), string(got)) {
		t.Fatalf("\nwant: %q\ngot:  %q", want, got)
	}
}
