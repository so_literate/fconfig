// Package hcl contains hashicorp config file decoder.
package hcl

import (
	"errors"
	"fmt"

	"github.com/hashicorp/hcl/v2/gohcl"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/hashicorp/hcl/v2/hclwrite"
)

// Decoder implements HCL FileDecoder in fconfig.
var Decoder = decoder{}

// decoder implements FileDecoder of the config.
type decoder struct{}

// Format returns ext of the supported file.
func (decoder) Formats() []string {
	return []string{".hcl"}
}

// DecodeFile reads and decodes file to config destination.
func (decoder) DecodeFile(path string, dst interface{}) error {
	if err := hclsimple.DecodeFile(path, nil, dst); err != nil {
		return fmt.Errorf("hclsimple.Decode: %w", err)
	}

	return nil
}

// ErrPanicAtHCL returns when hcl lib called panic.
var ErrPanicAtHCL = errors.New("panic at hcl lib")

// Marshal returns the HCL encoding of v.
func (decoder) Marshal(v interface{}) (res []byte, err error) {
	f := hclwrite.NewEmptyFile()

	defer func() {
		if rec := recover(); rec != nil {
			err = fmt.Errorf("%w: %v", ErrPanicAtHCL, rec)
		}
	}()

	gohcl.EncodeIntoBody(v, f.Body())

	return f.Bytes(), nil
}
