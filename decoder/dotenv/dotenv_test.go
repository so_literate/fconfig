package dotenv_test

import (
	"errors"
	"os"
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/decoder/dotenv"
)

func TestFormats(t *testing.T) {
	t.Parallel()

	formats := dotenv.Decoder.Formats()
	if !reflect.DeepEqual(formats, []string{".env"}) {
		t.Fatal(formats)
	}
}

func TestDecodeFile(t *testing.T) {
	t.Parallel()

	got := make(map[string]string)

	err := dotenv.Decoder.DecodeFile("./testdata/env.env", &got)
	if err != nil {
		t.Fatal(err)
	}

	want := map[string]string{
		"KEY":      "VALUE",
		"INT":      "5",
		"SUB_NAME": "sub-value",
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// file not found
	err = dotenv.Decoder.DecodeFile("./testdata/not_found.env", &got)
	if os.IsNotExist(err) {
		t.Fatal(err)
	}

	// wrong type file
	err = dotenv.Decoder.DecodeFile("./testdata/env.env", struct{}{})
	if !errors.Is(err, dotenv.ErrWrongDestinationType) {
		t.Fatal(err)
	}
}

func TestMarshal(t *testing.T) {
	t.Parallel()

	_, err := dotenv.Decoder.Marshal(nil)
	if !errors.Is(err, dotenv.ErrUnimplementedMarshal) {
		t.Fatal(err)
	}
}
