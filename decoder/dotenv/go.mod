module gitlab.com/so_literate/fconfig/decoder/dotenv

go 1.17

require github.com/joho/godotenv v1.3.0
