module gitlab.com/so_literate/fconfig/decoder/toml

go 1.17

require github.com/pelletier/go-toml v1.9.3
