package toml_test

import (
	"io/ioutil"
	"os"
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/decoder/toml"
)

func TestFormats(t *testing.T) {
	t.Parallel()

	formats := toml.Decoder.Formats()
	if !reflect.DeepEqual(formats, []string{".toml"}) {
		t.Fatal(formats)
	}
}

type File struct {
	Int    int    `toml:"int"`
	String string `toml:"string"`
	Slice  []int
	Struct struct {
		Field string
	} `toml:"struct"`
	Empty int `toml:",omitempty"`
}

func TestDecodeFile(t *testing.T) {
	t.Parallel()

	got := File{}

	err := toml.Decoder.DecodeFile("./testdata/file.toml", &got)
	if err != nil {
		t.Fatal(err)
	}

	want := File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	want.Struct.Field = "sub field"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// file not found
	err = toml.Decoder.DecodeFile("./testdata/not_found.toml", &got)
	if os.IsNotExist(err) {
		t.Fatal(err)
	}

	// broken dst file
	err = toml.Decoder.DecodeFile("./testdata/broken.toml", &got)
	if err == nil {
		t.Fatal("empty error on broken file")
	}
}

func TestMarshal(t *testing.T) {
	t.Parallel()

	val := &File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	val.Struct.Field = "sub field"

	got, err := toml.Decoder.Marshal(val)
	if err != nil {
		t.Fatal(err)
	}

	want, err := ioutil.ReadFile("./testdata/file.toml")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(string(want), string(got)) {
		t.Fatalf("\nwant: %q\ngot:  %q", want, got)
	}

	_, err = toml.Decoder.Marshal(make(chan int))
	if err == nil {
		t.Fatal("empty error on wrong type")
	}
}
