// Package yaml contains YAML config file decoder.
package yaml

import (
	"fmt"
	"os"

	yaml "github.com/goccy/go-yaml"
)

// Decoder implements YAML FileDecoder in fconfig.
var Decoder = decoder{}

// decoder implements FileDecoder of the config.
type decoder struct{}

// Format returns ext of the supported file.
func (decoder) Formats() []string {
	return []string{".yml", ".yaml"}
}

// DecodeFile reads and decodes file to config destination.
func (decoder) DecodeFile(path string, dst interface{}) error {
	file, err := os.Open(path) // nolint:gosec // it's ok with Potential file inclusion via variable.
	if err != nil {
		return fmt.Errorf("os.Open: %w", err)
	}

	defer file.Close() // nolint:errcheck,gosec // don't care about this error.

	if err = yaml.NewDecoder(file).Decode(dst); err != nil {
		return fmt.Errorf("yaml.Decode: %w", err)
	}

	return nil
}

// Marshal returns the YAML encoding of v.
func (decoder) Marshal(v interface{}) ([]byte, error) {
	return yaml.MarshalWithOptions(v, yaml.Indent(2), yaml.IndentSequence(true)) // nolint:wrapcheck // no need in an additional context.
}
