package json_test

import (
	"encoding/json"
	"errors"
	"os"
	"reflect"
	"testing"

	decJSON "gitlab.com/so_literate/fconfig/decoder/json"
)

func TestFormats(t *testing.T) {
	t.Parallel()

	formats := decJSON.Decoder.Formats()
	if !reflect.DeepEqual(formats, []string{".json"}) {
		t.Fatal(formats)
	}
}

type File struct {
	Int    int    `json:"int"`
	String string `json:"string"`
	Slice  []int
	Struct struct {
		Field string
	} `json:"struct"`
	Empty int `json:",omitempty"`
}

func TestDecodeFile(t *testing.T) {
	t.Parallel()

	got := File{}

	err := decJSON.Decoder.DecodeFile("./testdata/file.json", &got)
	if err != nil {
		t.Fatal(err)
	}

	want := File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	want.Struct.Field = "sub field"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	// file not found
	err = decJSON.Decoder.DecodeFile("./testdata/not_found.json", &got)
	if os.IsNotExist(err) {
		t.Fatal(err)
	}

	// broken file
	err = decJSON.Decoder.DecodeFile("./testdata/broken.json", &got)
	expErr := new(json.SyntaxError)
	if !errors.As(err, &expErr) {
		t.Fatal(err)
	}
}

func TestMarshal(t *testing.T) {
	t.Parallel()

	val := &File{
		Int:    5,
		String: "text",
		Slice:  []int{1, 2},
	}
	val.Struct.Field = "sub field"

	got, err := decJSON.Decoder.Marshal(val)
	if err != nil {
		t.Fatal(err)
	}

	want, err := os.ReadFile("./testdata/file.json")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %q\ngot:  %q", want, got)
	}
}
