package fconfig_test

import (
	"fmt"

	"gitlab.com/so_literate/fconfig"
)

type Server struct {
	HTTPAddr string `default:":8080" usage:"address of the HTTP server"`
	Auth     struct {
		User string `required:"true" usage:"your user"`
		Pass string `required:"true" usage:"keep it in secret"`
	}
}

type SubConfig struct {
	Enum string `default:"value" enum:"value,other value" usage:"must be one of the list"`
}

type Embedded struct {
	EmbeddedField string `env:"FIELD" flag:"field" json:"field" usage:"field with special name"`
}

type Config struct {
	Server   *Server   // ENV: SERVER_HTTP_ADDR, SERVER_AUTH_USER. flags: server.http_addr, server.auth.user
	Sub      SubConfig // ENV: SUB_ENUM. flag: sub.enum
	Embedded           // ENV: FIELD. flag: field

	Values []int `enum:"[1 2],[3 4]" usage:"supported enums with slice too"`
	Empty  string
}

func Example() {
	conf := new(Config)

	err := fconfig.New(conf).
		SkipFlags().                        // feel free to skip some steps.
		AddFile("./testdata/example.json"). // also you can add non-existent files (see FileNotFoundAlert).
		Load()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("HTTPAddr: %v\n", conf.Server.HTTPAddr) // conf.Server is not nil always.
	fmt.Printf("Auth.User: %v\n", conf.Server.Auth.User)
	fmt.Printf("Auth.Pass: %v\n", conf.Server.Auth.Pass)
	fmt.Printf("Sub.Enum: %v\n", conf.Sub.Enum)
	fmt.Printf("EmbeddedField: %v\n", conf.EmbeddedField)
	fmt.Printf("Values: %v\n", conf.Values)
	fmt.Printf("Empty: %q\n", conf.Empty)

	// Output:
	//
	// HTTPAddr: :8080
	// Auth.User: server-auth-user
	// Auth.Pass: server-auth-pass
	// Sub.Enum: other value
	// EmbeddedField: value of EmbeddedField
	// Values: [1 2]
	// Empty: ""
}
