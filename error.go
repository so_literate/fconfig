package fconfig

import (
	"errors"
)

var (
	// ErrAllSourcesSkipped returns when all config loader sources are skipped.
	ErrAllSourcesSkipped = errors.New("all config sources are skipped")
	// ErrInvalidConfigDestination returns when config destination can't be used
	// as destination value to parse.
	ErrInvalidConfigDestination = errors.New("invalid config destination")
	// ErrUnsupportedFieldType returns when field processor got field with unsupported type.
	ErrUnsupportedFieldType = errors.New("unknown type of the field")
	// ErrDecoderNotFound returns when decoder for file not found.
	ErrDecoderNotFound = errors.New("decoder not found (see AddFileDecoder)")
	// ErrUnknownEnvVariable returns when env parser found unknown env var and
	// config contains PrefixEnv without AllowUnknownEnvs flag.
	ErrUnknownEnvVariable = errors.New("unknown environment variable (see AllowUnknownEnvs)")
	// ErrEmptyRequiredField returns when field marked as required is not seted after load sources.
	ErrEmptyRequiredField = errors.New("field is required to set")
	// ErrNotEnumValue returns when value of the field is not one of enum list.
	ErrNotEnumValue = errors.New("field value must be one of enum")
)
