.PHONY: lints lints_fix test coverage

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml

test:
	cd ./decoder/dotenv && go test ./...
	cd ./decoder/hcl && go test ./...
	cd ./decoder/toml && go test ./...
	cd ./decoder/yaml && go test ./...
	go test -cover ./...

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov
	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

