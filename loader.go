package fconfig

import (
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/util"
)

// List of the supported tags of the config field.
// Also you can set tags of the file decoder: 'json', 'yml', 'yaml', etc...
const (
	// TagDefault default value of the field.
	//  Field int `default:"1"`, `default:"value"`
	TagDefault = "default"
	// TagUsage description of field usage.
	//  Field int `usage:"description of field usage"`
	TagUsage = "usage"
	// TagRequired field will checked as required.
	//  Field int `required:"true"`, `required:"false"` or use AllFieldsRequired.
	TagRequired = "required"
	// TagNameFlag name of the flag.
	//  Field int `flag:"flag_name"`
	TagNameFlag = "flag"
	// TagNameENV name of the environment variable.
	//  Field int `env:"ENV_NAME"`
	TagNameENV = "env"
	// TagEnum allowed values of the field.
	//  Field string `enum:"value1,value2"`
	TagEnum = "enum"
)

var tags = []string{TagDefault, TagUsage, TagRequired, TagNameFlag, TagNameENV, TagEnum}

func (l *Loader) init() error {
	if !util.IsAllocatedPointerToStruct(l.Destination) {
		return fmt.Errorf("%w: config destination must be not nil pointer to struct", ErrInvalidConfigDestination)
	}

	return nil
}

func (l *Loader) loadSources() error {
	if err := l.loadDefaults(); err != nil {
		return fmt.Errorf("defaults: %w", err)
	}

	if err := l.loadFiles(); err != nil {
		return fmt.Errorf("files: %w", err)
	}

	if err := l.loadEnv(); err != nil {
		return fmt.Errorf("environment variables: %w", err)
	}

	l.loadFlags()

	return nil
}

func (l *Loader) load() error {
	if l.Config.SkipDefaults && l.Config.SkipFiles && l.Config.SkipEnv && l.Config.SkipFlags {
		return ErrAllSourcesSkipped
	}

	if l.Config.Output == nil {
		l.Config.Output = os.Stdout
	}

	if l.Config.Flags == nil {
		l.Config.Flags = flag.NewFlagSet(l.Config.PrefixFlag, flag.ContinueOnError)
		l.Config.Flags.SetOutput(l.Config.Output)
	}

	if l.Config.FlagArgs == nil {
		l.Config.FlagArgs = os.Args[1:]
	}

	l.cleanDestination = util.AllocCopyValue(l.Destination)

	l.fields = analyzer.New(true, tags...).GetFields(l.Destination)
	if len(l.fields) == 0 {
		return fmt.Errorf("%w: no field to parse", ErrInvalidConfigDestination)
	}

	// have to set and parse flags before loading other sources.
	if err := l.setFlags(); err != nil {
		return fmt.Errorf("setFlags: %w", err)
	}

	if err := l.parseFlags(); err != nil {
		if errors.Is(err, flag.ErrHelp) && !l.Config.FlagContinueOnHelp {
			os.Exit(0) // already printed default values just exit now.
		}

		return fmt.Errorf("failed to parse flags: %w", err)
	}

	if err := l.loadSources(); err != nil {
		return fmt.Errorf("failed to load %w", err)
	}

	if err := l.checkFields(); err != nil {
		return fmt.Errorf("failed to check fields: %w", err)
	}

	return nil
}
