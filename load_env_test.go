package fconfig_test

import (
	"errors"
	"os"
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/so_literate/fconfig"
)

func TestLoadEnv(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int
		String string
		Slice  []int64 `env:"Slice"`
		Struct struct {
			SField string
		}
		Empty int
	}

	checkErr := func(err error) {
		t.Helper()
		if err != nil {
			t.Fatal(err)
		}
	}

	checkErr(os.Setenv("INT", "5"))
	checkErr(os.Setenv("STRING", "text"))
	checkErr(os.Setenv("Slice", "1,2"))
	checkErr(os.Setenv("STRUCT_S_FIELD", "=sub field text="))

	got := config{}

	loader := fconfig.New(&got).SkipDefaults().SkipFiles().SkipFlags()
	checkErr(loader.Load())

	want := config{
		Int:    5,
		String: "text",
		Slice:  []int64{1, 2},
	}
	want.Struct.SField = "=sub field text="

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	checkErr(os.Setenv("TEST-INTEGER", "5"))

	err := loader.PrefixEnv("TEST").DelimiterEnv("-").Load()
	if !errors.Is(err, fconfig.ErrUnknownEnvVariable) {
		t.Fatal(err)
	}

	checkErr(loader.PrefixEnv("TEST").AllowUnknownEnvs().Load())

	checkErr(os.Setenv("INT", "text"))
	err = loader.PrefixEnv("").Load()
	if !errors.Is(err, strconv.ErrSyntax) {
		t.Fatal(err)
	}
}

func TestLoadEnv_Delimiters(t *testing.T) {
	t.Parallel()

	type config struct {
		Parent struct {
			Child       string
			ChildSecond string
		}
	}

	got := &config{}
	want := &config{}

	checkErr := func(err error) {
		t.Helper()
		if err != nil {
			t.Fatal(err)
		}
	}

	assert := func() {
		t.Helper()
		if !reflect.DeepEqual(*want, *got) {
			t.Fatalf("\nwant: %#v\ngot:  %#v", *want, *got)
		}
	}

	loader := fconfig.New(got).SkipDefaults().SkipFiles().SkipFlags().PrefixEnv("TESTDELIMITERS")

	checkErr(os.Setenv("TESTDELIMITERS_PARENT_CHILD", "test-1"))
	checkErr(os.Setenv("TESTDELIMITERS_PARENT_CHILD_SECOND", "test-1"))

	want.Parent.Child = "test-1"
	want.Parent.ChildSecond = "test-1"

	checkErr(loader.Load())
	assert()

	// Set custom delimiters of env.
	loader.DelimiterEnv("-").DelimiterEnvWords("")

	checkErr(os.Setenv("TESTDELIMITERS-PARENT-CHILD", "test-2"))
	checkErr(os.Setenv("TESTDELIMITERS-PARENT-CHILDSECOND", "test-2"))

	want.Parent.Child = "test-2"
	want.Parent.ChildSecond = "test-2"

	checkErr(loader.Load())
	assert()
}
