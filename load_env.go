package fconfig

import (
	"fmt"

	"gitlab.com/so_literate/fconfig/internal/model"
	"gitlab.com/so_literate/fconfig/internal/util"
)

func (l *Loader) getEnvName(field *model.Field) string {
	if envName, ok := field.GetTag(TagNameENV); ok {
		return envName
	}

	return field.GetName(l.Config.PrefixEnv, l.Config.DelimiterEnvWords, l.Config.DelimiterEnv, model.CaseNameModUpper)
}

func (l *Loader) loadEnv() error {
	if l.Config.SkipEnv {
		return nil
	}

	envPrefix := l.Config.PrefixEnv
	if envPrefix != "" {
		envPrefix += l.Config.DelimiterEnv
	}

	env := util.GetEnv(envPrefix)

	for _, field := range l.fields {
		envName := l.getEnvName(field)

		value, ok := env[envName]
		if !ok {
			continue
		}

		if err := util.SetFieldValue(field, value); err != nil {
			return fmt.Errorf("failed to set field value: %s: %w", field.String(), err)
		}

		delete(env, envName)
	}

	if !l.Config.AllowUnknownEnvs && l.Config.PrefixEnv != "" {
		if len(env) != 0 {
			for name, value := range env {
				return fmt.Errorf("%w: %s=%s", ErrUnknownEnvVariable, name, value)
			}
		}
	}

	return nil
}
