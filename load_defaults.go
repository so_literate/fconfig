package fconfig

import (
	"fmt"

	"gitlab.com/so_literate/fconfig/internal/util"
)

func (l *Loader) loadDefaults() error {
	if l.Config.SkipDefaults {
		return nil
	}

	for _, field := range l.fields {
		value, ok := field.GetTag(TagDefault)
		if !ok || value == "" {
			continue
		}

		err := util.SetFieldValue(field, value)
		if err != nil {
			return fmt.Errorf("failed to Set field value: %s: %w", field.String(), err)
		}
	}

	return nil
}
