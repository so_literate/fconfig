package fconfig_test

import (
	"bytes"
	"errors"
	"flag"
	"reflect"
	"strconv"
	"testing"
	"time"

	"gitlab.com/so_literate/fconfig"
)

func TestLoadFlags(t *testing.T) {
	t.Parallel()

	type config struct {
		Int    int    `default:"1" enum:"1,3,5,7" usage:"decription"`
		String string `default:"default-text"`
		Bool   bool   `usage:"decription"`
		BoolF  bool
		Float  float64
		Dur    time.Duration `enum:"1s,10s"`
		Int64  int64
		Uint   uint
		Slice  []int64 `flag:"Slice"`
		Struct struct {
			SField string
		}
		Empty int
	}

	got := config{}

	out := bytes.NewBuffer(nil)

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().SkipFiles().SetOutput(out).FlagContinueOnHelp()
	loader.Config.FlagArgs = []string{
		`-int=5`,
		`-string=text`,
		`-bool`,
		`-float=1.1`,
		`-dur=1s`,
		`-int_64=1`,
		`-uint=1`,
		`-Slice=1,2`,
		`-struct.s_field=sub-field -t ext`,
	}

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{
		Int:    5,
		String: "text",
		Bool:   true,
		Float:  1.1,
		Dur:    time.Second,
		Int64:  1,
		Uint:   1,
		Slice:  []int64{1, 2},
	}
	want.Struct.SField = "sub-field -t ext"

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}

	loader.Config.Flags = nil
	loader.Config.FlagArgs = []string{`-h`}
	err = loader.Load()
	if !errors.Is(err, flag.ErrHelp) {
		t.Fatal(err)
	}

	// Too many spaces and tabs, be careful.
	helpText := `Usage:
  -Slice string
    	
  -bool
    	decription
  -bool_f
    	
  -dur duration
    	allowed: 1s,10s
  -empty int
    	
  -float float
    	
  -int int
    	decription (allowed: 1,3,5,7) (default 1)
  -int_64 int
    	
  -string string
    	 (default "default-text")
  -struct.s_field string
    	
  -uint uint
    	
`

	if out.String() != helpText {
		t.Fatalf("%q", out.String())
	}
}

func TestLoadFlags_PrefixAndDelimiter(t *testing.T) {
	t.Parallel()

	type config struct {
		Struct struct {
			IntValue int
		}
	}

	got := config{}

	loader := fconfig.New(&got).SkipDefaults().SkipEnv().SkipFiles().PrefixFlag("app").DelimiterFlag("_").DelimiterFlagWords(".")
	loader.Config.FlagArgs = []string{"-app_struct_int.value=5"}

	err := loader.Load()
	if err != nil {
		t.Fatal(err)
	}

	want := config{}
	want.Struct.IntValue = 5

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("\nwant: %#v\ngot:  %#v", want, got)
	}
}

func TestSetFlags_BadCases(t *testing.T) {
	t.Parallel()

	testCase := func(dst interface{}, wantErr error) {
		t.Helper()

		err := fconfig.New(dst).SkipDefaults().SkipEnv().SkipFiles().Load()
		if !errors.Is(err, wantErr) {
			t.Fatal(err)
		}
	}

	type badInt struct {
		Value int `default:"text"`
	}
	testCase(new(badInt), strconv.ErrSyntax)

	type badBool struct {
		Value bool `default:"text"`
	}
	testCase(new(badBool), strconv.ErrSyntax)

	type badInt64 struct {
		Value int64 `default:"text"`
	}
	testCase(new(badInt64), strconv.ErrSyntax)

	type badUint64 struct {
		Value uint64 `default:"text"`
	}
	testCase(new(badUint64), strconv.ErrSyntax)

	type badFloat64 struct {
		Value float64 `default:"text"`
	}
	testCase(new(badFloat64), strconv.ErrSyntax)

	type badDur struct {
		Dur time.Duration `default:"1d"`
	}
	err := fconfig.New(new(badDur)).SkipDefaults().SkipEnv().SkipFiles().Load()
	if err == nil {
		t.Fatal(err)
	}
}
