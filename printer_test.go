package fconfig_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"testing"
	"time"

	"gitlab.com/so_literate/fconfig"
	decJSON "gitlab.com/so_literate/fconfig/decoder/json"
)

type printConfig struct {
	Int    int `default:"5"`
	Struct struct {
		SubField string `default:"http://some-url.org/path"`
	}
	String string        `default:"text"`
	Dur    time.Duration `default:"1s" json:"not_string"`
	Empty  int
}

func testCasePrinter(
	t *testing.T,
	loader *fconfig.Loader,
	printFn func(out ...io.Writer) error,
	expErr error,
	expMessage string,
) {
	t.Helper()

	got := printConfig{}
	out := bytes.NewBuffer(nil)

	l := fconfig.New(&got).FileFlagConfig("config")
	oldConfig := *l.Config

	*loader = *l // replace parent loader

	err := printFn(out)
	if !errors.Is(err, expErr) {
		t.Fatalf("err:\nwant: %s\ngot:  %s", expErr, err)
	}

	want := printConfig{}

	if !reflect.DeepEqual(want, got) { // keep dst empty.
		t.Fatalf("dst:\nwant: %#v\ngot:  %#v", want, got)
	}

	if !reflect.DeepEqual(oldConfig, *loader.Config) { // keep origin config.
		t.Fatalf("conf:\nwant: %#v\ngot:  %#v", oldConfig, *loader.Config)
	}

	if expMessage != out.String() {
		t.Fatalf("%q", out.String())
	}

	out.Reset()

	// Bad cases:

	loader.Destination = printConfig{}
	err = printFn(out)
	if !errors.Is(err, fconfig.ErrInvalidConfigDestination) {
		t.Fatalf("err bad dst:\nwant: %s\ngot:  %s", fconfig.ErrInvalidConfigDestination, err)
	}

	type emptyConf struct{}
	loader.Destination = &emptyConf{}
	loader.Config.Output = out
	err = printFn()
	if !errors.Is(err, fconfig.ErrInvalidConfigDestination) {
		t.Fatalf("err bad dst2:\nwant: %s\ngot:  %s", fconfig.ErrInvalidConfigDestination, err)
	}

	type badConfig struct {
		Int int `default:"text"`
	}

	loader.Config.Output = nil
	loader.Destination = &badConfig{}
	err = printFn()
	if !errors.Is(err, strconv.ErrSyntax) {
		t.Fatalf("err bad default:\nwant: %s\ngot:  %s", strconv.ErrSyntax, err)
	}
}

func TestPrintFlagDefaults(t *testing.T) {
	t.Parallel()

	expMessage := `  -config string
    	path to config file
  -dur duration
    	 (default 1s)
  -empty int
    	
  -int int
    	 (default 5)
  -string string
    	 (default "text")
  -struct.sub_field string
    	 (default "http://some-url.org/path")
`

	loader := fconfig.New(nil)

	testCasePrinter(t, loader, loader.PrintFlagDefaults, nil, expMessage)

	type badConfig struct {
		Int int `default:"text"`
	}

	err := fconfig.New(&badConfig{}).SkipDefaults().PrintFlagDefaults()
	if !errors.Is(err, strconv.ErrSyntax) {
		t.Fatalf("\nwant: %s\ngot:  %s", strconv.ErrSyntax, err)
	}
}

func TestPrintEnvDefaults(t *testing.T) {
	t.Parallel()

	expMessage := `INT="5"
STRUCT_SUB_FIELD="http://some-url.org/path"
STRING="text"
DUR="1s"
EMPTY=""
`

	loader := fconfig.New(nil)

	testCasePrinter(t, loader, loader.PrintEnvDefaults, nil, expMessage)
}

func TestPrintEnvK8SDefaults(t *testing.T) {
	t.Parallel()

	expMessage := `- { name: INT,                                    value: "5" }
- { name: STRUCT_SUB_FIELD,                       value: "http://some-url.org/path" }
- { name: STRING,                                 value: "text" }
- { name: DUR,                                    value: "1s" }
- { name: EMPTY,                                  value: "" }
`

	loader := fconfig.New(nil)

	testCasePrinter(t, loader, loader.PrintEnvK8SDefaults, nil, expMessage)
}

func TestPrintFileJSON(t *testing.T) {
	t.Parallel()

	got := printConfig{}
	out := bytes.NewBuffer(nil)

	loader := fconfig.New(&got)
	oldConfig := *loader.Config

	err := loader.PrintFileJSON(out)
	if err != nil {
		t.Fatal(err)
	}

	want := printConfig{}

	if !reflect.DeepEqual(want, got) { // keep dst empty.
		t.Fatalf("dst:\nwant: %#v\ngot:  %#v", want, got)
	}

	if !reflect.DeepEqual(oldConfig, *loader.Config) { // keep origin config.
		t.Fatalf("conf:\nwant: %#v\ngot:  %#v", oldConfig, *loader.Config)
	}

	expMessage := `{
  "Int": 5,
  "Struct": {
    "SubField": "http://some-url.org/path"
  },
  "String": "text",
  "not_string": 1000000000,
  "Empty": 0
}
`

	if expMessage != out.String() {
		t.Fatalf("%q", out.String())
	}

	// Bad cases:

	type emptyConf struct{}
	err = fconfig.New(new(emptyConf)).PrintFileJSON()
	if !errors.Is(err, fconfig.ErrInvalidConfigDestination) {
		t.Fatalf("\nwant: %s\ngot:  %s", fconfig.ErrInvalidConfigDestination, err)
	}

	type badConfig struct {
		Int  int
		Chan chan int // it's ok for fconfig but not for json marshaller.
	}
	err = fconfig.New(new(badConfig)).PrintFileJSON()
	wantErr := new(json.UnsupportedTypeError)
	if !errors.As(err, &wantErr) {
		t.Fatalf("\nwant: %s\ngot:  %s", wantErr, err)
	}
}

type Service struct {
	HTTPAddr string `default:":8080" usage:"address of the HTTP server"`
	Auth     struct {
		User string `required:"true" usage:"your user"`
		Pass string `required:"true" usage:"keep it in secret"`
	}
}

type AppConfig struct {
	Service *Service
	Root    string `default:"DEFAULT"`
	Enum    int    `enum:"1,2"`
	EnumDef int    `default:"1" enum:"1,2"`
}

func ExampleLoader_PrintFlagDefaults() {
	if err := fconfig.New(new(AppConfig)).PrintFlagDefaults(); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// 	 -enum int
	//     	allowed: 1,2
	//   -enum_def int
	//     	allowed: 1,2 (default 1)
	//   -root string
	//     	 (default "DEFAULT")
	//   -service.auth.pass string
	//     	keep it in secret
	//   -service.auth.user string
	//     	your user
	//   -service.http_addr string
	//     	address of the HTTP server (default ":8080")
}

func ExampleLoader_PrintEnvDefaults() {
	if err := fconfig.New(new(AppConfig)).PrintEnvDefaults(); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// SERVICE_HTTP_ADDR=":8080"
	// SERVICE_AUTH_USER=""
	// SERVICE_AUTH_PASS=""
	// ROOT="DEFAULT"
	// ENUM=""
	// ENUM_DEF="1"
}

func ExampleLoader_PrintEnvK8SDefaults() {
	if err := fconfig.New(new(AppConfig)).PrintEnvK8SDefaults(); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// - { name: SERVICE_HTTP_ADDR,                      value: ":8080" }
	// - { name: SERVICE_AUTH_USER,                      value: "" }
	// - { name: SERVICE_AUTH_PASS,                      value: "" }
	// - { name: ROOT,                                   value: "DEFAULT" }
	// - { name: ENUM,                                   value: "" }
	// - { name: ENUM_DEF,                               value: "1" }
}

func ExampleLoader_PrintFileDefaults() {
	if err := fconfig.New(new(AppConfig)).PrintFileDefaults(decJSON.Decoder); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// {
	//   "Service": {
	//     "HTTPAddr": ":8080",
	//     "Auth": {
	//       "User": "",
	//       "Pass": ""
	//     }
	//   },
	//   "Root": "DEFAULT",
	//   "Enum": 0,
	//   "EnumDef": 1
	// }
}

func ExampleLoader_PrintFileDefaults_skipDefault() {
	if err := fconfig.New(new(AppConfig)).SkipDefaults().PrintFileDefaults(decJSON.Decoder); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// {
	//   "Service": {
	//     "HTTPAddr": "",
	//     "Auth": {
	//       "User": "",
	//       "Pass": ""
	//     }
	//   },
	//   "Root": "",
	//   "Enum": 0,
	//   "EnumDef": 0
	// }
}

func ExampleLoader_PrintFileJSON() {
	if err := fconfig.New(new(AppConfig)).PrintFileJSON(); err != nil {
		fmt.Println(err)
	}
	// Output:
	//
	// {
	//   "Service": {
	//     "HTTPAddr": ":8080",
	//     "Auth": {
	//       "User": "",
	//       "Pass": ""
	//     }
	//   },
	//   "Root": "DEFAULT",
	//   "Enum": 0,
	//   "EnumDef": 1
	// }
}
