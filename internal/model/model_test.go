package model_test

import (
	"testing"

	"gitlab.com/so_literate/fconfig/internal/model"
)

func TestField_String(t *testing.T) {
	t.Parallel()

	field := &model.Field{
		NameParts: [][]string{{"Parent", "Part"}, {"Child", "Part"}},
	}

	res := field.String()

	if res != "ParentPart.ChildPart" {
		t.Fatal(res)
	}

	envName := field.GetName("PREFIX", "_", "_", model.CaseNameModUpper)
	if envName != "PREFIX_PARENT_PART_CHILD_PART" {
		t.Fatal(envName)
	}

	flagName := field.GetName("", "_", ".", model.CaseNameModLower)
	if flagName != "parent_part.child_part" {
		t.Fatal(flagName)
	}
}

func TestField_GetTag(t *testing.T) {
	t.Parallel()

	field := &model.Field{
		Tags: map[string]string{
			"key": "value",
		},
	}

	value, ok := field.GetTag("key")
	if !ok {
		t.Fatal(ok)
	}

	if value != "value" {
		t.Fatal(value)
	}

	value, ok = field.GetTag("tag")
	if ok {
		t.Fatal(ok)
	}

	if value != "" {
		t.Fatal(value)
	}
}
