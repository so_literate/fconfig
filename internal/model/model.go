// Package model contains common models of the project.
package model

import (
	"reflect"
	"strings"
)

// Field of the destination struct.
type Field struct {
	NameParts [][]string        // Parts of the field name with parent parts. [[parent,parts][child,parts]]
	Tags      map[string]string // Collection of the field tags.
	IsSet     bool              // Means that the value is already seted.
	Value     reflect.Value     // Reflect value of the field.
	Type      reflect.Type      // Reflect type of the field.
}

// CaseNameMod is a type of modification name case.
type CaseNameMod int

// Vairiants of the modification.
const (
	CaseNameModKeep CaseNameMod = iota
	CaseNameModUpper
	CaseNameModLower
)

// GetName returns name of the field with prefix delimeter and modificated case.
func (f *Field) GetName(prefix, nameDelimeter, partDelimeter string, caseMod CaseNameMod) string {
	nameParts := make([]string, len(f.NameParts))

	for i, parts := range f.NameParts {
		nameParts[i] = strings.Join(parts, nameDelimeter)
	}

	name := strings.Join(nameParts, partDelimeter)

	switch caseMod {
	case CaseNameModUpper:
		name = strings.ToUpper(name)
	case CaseNameModLower:
		name = strings.ToLower(name)
	case CaseNameModKeep:
	}

	if prefix != "" {
		return prefix + partDelimeter + name
	}

	return name
}

// String representation of the Field.
func (f *Field) String() string {
	return f.GetName("", "", ".", CaseNameModKeep)
}

// GetTag returns value of the field tag.
func (f *Field) GetTag(tag string) (value string, ok bool) {
	value, ok = f.Tags[tag]
	return value, ok
}
