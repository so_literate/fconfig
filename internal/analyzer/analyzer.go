// Package analyzer contains methods to process fields of the destination config value.
package analyzer

import (
	"reflect"

	"gitlab.com/so_literate/fconfig/internal/model"
	"gitlab.com/so_literate/fconfig/internal/util"
)

var supportedKinds = map[reflect.Kind]struct{}{
	reflect.Bool: {},
	reflect.Int:  {}, reflect.Int8: {}, reflect.Int16: {}, reflect.Int32: {}, reflect.Int64: {},
	reflect.Uint: {}, reflect.Uint8: {}, reflect.Uint16: {}, reflect.Uint32: {}, reflect.Uint64: {},
	reflect.Float32: {}, reflect.Float64: {},
	reflect.String: {},

	// not supported in slice.
	reflect.Slice:  {},
	reflect.Struct: {},
}

// Analyzer process fields of the struct.
type Analyzer struct {
	AllocNilField bool     // Will alloc nil pointer fields.
	Tags          []string // List of the tags to parse.
}

// New returns new Analyzer based on your tags.
func New(allocNilField bool, tags ...string) *Analyzer {
	return &Analyzer{
		AllocNilField: allocNilField,
		Tags:          tags,
	}
}

func (a *Analyzer) newField(field *reflect.StructField, value reflect.Value, parent *model.Field) *model.Field {
	var parentNameParts [][]string
	if parent != nil {
		parentNameParts = make([][]string, len(parent.NameParts), len(parent.NameParts)+1)
		copy(parentNameParts, parent.NameParts)
	}

	res := &model.Field{
		NameParts: append(parentNameParts, util.SplitCamelCaseByWords(field.Name)),
		Tags:      make(map[string]string),
		IsSet:     !util.IsValueZero(value),
		Value:     value,
		Type:      field.Type,
	}

	for _, tag := range a.Tags {
		value, ok := field.Tag.Lookup(tag)
		if ok {
			res.Tags[tag] = value
		}
	}

	return res
}

func isKindSupported(kind reflect.Kind, tp reflect.Type) bool {
	// supported kinds of the field.
	if _, ok := supportedKinds[kind]; !ok {
		return false
	}

	// supported kinds of the slice field.
	if kind == reflect.Slice {
		kind = tp.Elem().Kind()
		_, ok := supportedKinds[kind]
		if !ok || kind == reflect.Slice || kind == reflect.Struct {
			return false
		}
	}

	return true
}

// nolint:gocognit,gocognit // it's pain but ok.
func (a *Analyzer) getFields(valueStruct reflect.Value, parent *model.Field) []*model.Field {
	tp := valueStruct.Type()
	count := valueStruct.NumField()

	res := make([]*model.Field, 0, count)

	for i := 0; i < count; i++ {
		value := valueStruct.Field(i)
		field := tp.Field(i)

		if value.Kind() == reflect.Ptr { // unwrap pointer.
			if value.IsNil() { // alloc new value.
				if !value.CanSet() { // skip field when nil value is unsetable.
					continue
				}

				if a.AllocNilField {
					value.Set(reflect.New(value.Type().Elem()))
				} else {
					value = reflect.New(value.Type().Elem())
				}
			}

			value = value.Elem()
		}

		if !value.CanSet() {
			continue
		}

		f := a.newField(&field, value, parent)

		kind := field.Type.Kind()

		if kind == reflect.Ptr { // unwrap pointer kind.
			kind = field.Type.Elem().Kind()
		}

		if !isKindSupported(kind, field.Type) {
			continue
		}

		if kind == reflect.Struct { // recursive to get fields of the sub struct.
			var subParent *model.Field
			if field.Anonymous {
				subParent = parent
			} else {
				subParent = f
			}

			res = append(res, a.getFields(value, subParent)...)
			continue
		}

		res = append(res, f)
	}

	return res
}

// GetFields returns fields list of the strcut.
// Value must be pointer to struct.
func (a *Analyzer) GetFields(value interface{}) []*model.Field {
	return a.getFields(reflect.ValueOf(value).Elem(), nil)
}
