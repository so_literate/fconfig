package analyzer_test

import (
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/model"
)

type SubSubStruct struct {
	SSField int `tag:"value" default:"1" unknown:"tag"`
}

type EmbeddedStruct struct {
	EmbField int
}

type EmbeddedPointerStruct struct {
	EmbPointerField int
}

type SubStruct struct {
	SField  int
	SStruct *SubSubStruct
	SAnon   struct {
		FieldOfAnon int
	}

	EmbeddedStruct
	*EmbeddedPointerStruct
}

// nolint:unused // it's used to test skipper of the unexported types.
type unexportedStruct struct {
	USField int
}

type Config struct {
	Field  int
	Struct SubStruct
	Slice  []int

	unexportedField    int               // nolint:unused,structcheck // it's used to test skipper of the unexported types.
	ustruct            *unexportedStruct // nolint:unused,structcheck // it's used to test skipper of the unexported types.
	UnsupporField      map[string]string
	UnsupporFieldSlice [][]int
}

var expectedFields = []*model.Field{
	{
		NameParts: [][]string{{"Field"}},
		Tags:      map[string]string{},
	},
	{
		NameParts: [][]string{{"Struct"}, {"S", "Field"}},
		Tags:      map[string]string{},
	},
	{
		NameParts: [][]string{{"Struct"}, {"S", "Struct"}, {"SS", "Field"}},
		Tags:      map[string]string{"tag": "value", "default": "1"},
	},
	{
		NameParts: [][]string{{"Struct"}, {"S", "Anon"}, {"Field", "Of", "Anon"}},
		Tags:      map[string]string{},
	},
	{
		NameParts: [][]string{{"Struct"}, {"Emb", "Field"}},
		Tags:      map[string]string{},
	},
	{
		NameParts: [][]string{{"Struct"}, {"Emb", "Pointer", "Field"}},
		Tags:      map[string]string{},
	},
	{
		NameParts: [][]string{{"Slice"}},
		Tags:      map[string]string{},
	},
}

// nolint:gocritic // just for test it's ok.
func assertFields(t *testing.T, got, want model.Field) {
	t.Helper()

	if !reflect.DeepEqual(want.NameParts, got.NameParts) {
		t.Fatalf("NameParts: want %v, got %v", want.NameParts, got.NameParts)
	}

	if !reflect.DeepEqual(want.Tags, got.Tags) {
		t.Fatalf("Tags: want %v, got %v", want.Tags, got.Tags)
	}

	if want.IsSet != got.IsSet {
		t.Fatalf("IsSet: want %v, got %v", want.IsSet, got.IsSet)
	}
}

func TestGetFields(t *testing.T) {
	t.Parallel()

	value := &Config{}

	fields := analyzer.New(true, "tag", "default").GetFields(value)
	if len(fields) != len(expectedFields) {
		t.Fatalf("wrong number of the fields: %d != %d", len(fields), len(expectedFields))
	}

	expValue := &Config{
		Struct: SubStruct{
			SStruct:               new(SubSubStruct),
			EmbeddedPointerStruct: new(EmbeddedPointerStruct),
		},
	}

	if !reflect.DeepEqual(value, expValue) {
		t.Fatalf("unequal values: %v != %v", value, expValue)
	}

	assertFields(t, *fields[0], *expectedFields[0])
	assertFields(t, *fields[1], *expectedFields[1])
	assertFields(t, *fields[2], *expectedFields[2])
	assertFields(t, *fields[3], *expectedFields[3])
	assertFields(t, *fields[4], *expectedFields[4])
	assertFields(t, *fields[5], *expectedFields[5])
	assertFields(t, *fields[6], *expectedFields[6])
}

func TestGetFields_NotAlloc(t *testing.T) {
	t.Parallel()

	value := &Config{}

	fields := analyzer.New(false, "tag", "default").GetFields(value)
	if len(fields) != len(expectedFields) {
		t.Fatalf("wrong number of the fields: %d != %d", len(fields), len(expectedFields))
	}

	expValue := &Config{}

	if !reflect.DeepEqual(value, expValue) {
		t.Fatalf("unequal values: %v != %v", value, expValue)
	}

	assertFields(t, *fields[0], *expectedFields[0])
	assertFields(t, *fields[1], *expectedFields[1])
	assertFields(t, *fields[2], *expectedFields[2])
	assertFields(t, *fields[3], *expectedFields[3])
	assertFields(t, *fields[4], *expectedFields[4])
	assertFields(t, *fields[5], *expectedFields[5])
	assertFields(t, *fields[6], *expectedFields[6])
}

func TestGetFields_IsSet(t *testing.T) {
	t.Parallel()

	value := &Config{
		Struct: SubStruct{
			SStruct: &SubSubStruct{
				SSField: 1,
			},
		},
	}

	fields := analyzer.New(true, "tag", "default").GetFields(value)
	if len(fields) != len(expectedFields) {
		t.Fatalf("wrong number of the fields: %d != %d", len(fields), len(expectedFields))
	}

	expValue := &Config{
		Struct: SubStruct{
			SStruct: &SubSubStruct{
				SSField: 1,
			},
			EmbeddedPointerStruct: new(EmbeddedPointerStruct),
		},
	}

	if !reflect.DeepEqual(value, expValue) {
		t.Fatalf("unequal values: %v != %v", value, expValue)
	}

	assertFields(t, *fields[0], *expectedFields[0])
	assertFields(t, *fields[1], *expectedFields[1])

	expField2 := *expectedFields[2]
	expField2.IsSet = true
	assertFields(t, *fields[2], expField2)

	assertFields(t, *fields[3], *expectedFields[3])
	assertFields(t, *fields[4], *expectedFields[4])
	assertFields(t, *fields[5], *expectedFields[5])
	assertFields(t, *fields[6], *expectedFields[6])
}

func TestGetFields_Deep4(t *testing.T) {
	t.Parallel()

	type fourthLevel struct {
		One string
		Two string
	}

	type thirdLevel struct {
		Fourth fourthLevel
	}

	type secondLevel struct {
		ThirdLevel thirdLevel
	}

	type firstLevel struct {
		Second secondLevel
	}

	value := new(firstLevel)

	fields := analyzer.New(false).GetFields(value)
	if len(fields) != 2 {
		t.Fatalf("wrong length of the fields: %d", len(fields))
	}

	if fields[0].String() != "Second.ThirdLevel.Fourth.One" {
		t.Fatal(fields[0].String())
	}

	if fields[1].String() != "Second.ThirdLevel.Fourth.Two" {
		t.Fatal(fields[1].String())
	}
}
