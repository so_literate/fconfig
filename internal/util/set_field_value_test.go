package util_test

import (
	"errors"
	"reflect"
	"strconv"
	"testing"
	"time"

	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/model"
	"gitlab.com/so_literate/fconfig/internal/util"
)

func TestSetFieldValue(t *testing.T) {
	t.Parallel()

	testCase := func(field *model.Field, value string, wantErr error) {
		t.Helper()

		err := util.SetFieldValue(field, value)
		if !errors.Is(err, wantErr) {
			t.Fatalf("unexpected error: want %v, got %v\n\tdst:   %s\n\tvalue: %#[4]v(%[4]T)", wantErr, err, field.Value.String(), value)
		}
	}

	assertFieldValues := func(got interface{}, want interface{}) {
		t.Helper()

		if !reflect.DeepEqual(got, want) {
			t.Fatalf("field values not equals:\n\twant: %#[1]v(%[1]T)\n\tgot:  %#[2]v(%[2]T)", got, want)
		}
	}

	type Values struct {
		Bool     bool
		Int      int
		Int64    int64
		Dur      time.Duration
		Uint     uint
		Uint64   uint64
		Float64  float64
		String   string
		Bytes    []byte
		Slice    []int
		SliceStr []string
	}

	v := new(Values)

	fields := analyzer.New(true).GetFields(v)

	// Bool
	field := fields[0]
	testCase(field, "true", nil)
	assertFieldValues(v.Bool, true)
	testCase(field, "", nil)
	assertFieldValues(v.Bool, true)
	testCase(field, "false", nil)
	assertFieldValues(v.Bool, false)
	testCase(field, "false1", strconv.ErrSyntax)
	assertFieldValues(v.Bool, false)

	// Int
	field = fields[1]
	testCase(field, "1", nil)
	assertFieldValues(v.Int, 1)
	testCase(field, "2", nil)
	assertFieldValues(v.Int, 2)
	testCase(field, "2a", strconv.ErrSyntax)

	// Int64
	field = fields[2]
	testCase(field, "1", nil)
	assertFieldValues(v.Int64, int64(1))
	testCase(field, "2", nil)
	assertFieldValues(v.Int64, int64(2))
	testCase(field, "2a", strconv.ErrSyntax)

	// Duration
	field = fields[3]
	testCase(field, "1s", nil)
	assertFieldValues(v.Dur, time.Second)
	err := util.SetFieldValue(field, "2")
	if err == nil {
		t.Fatalf("empty error in parsing time.Duration")
	}

	// Uint
	field = fields[4]
	testCase(field, "1", nil)
	assertFieldValues(v.Uint, uint(1))
	testCase(field, "2", nil)
	assertFieldValues(v.Uint, uint(2))
	testCase(field, "2a", strconv.ErrSyntax)

	// Uint64
	field = fields[5]
	testCase(field, "1", nil)
	assertFieldValues(v.Uint64, uint64(1))
	testCase(field, "2", nil)
	assertFieldValues(v.Uint64, uint64(2))
	testCase(field, "2a", strconv.ErrSyntax)

	// Float64
	field = fields[6]
	testCase(field, "1.1", nil)
	assertFieldValues(v.Float64, 1.1)
	testCase(field, "2", nil)
	assertFieldValues(v.Float64, 2.0)
	testCase(field, "2a", strconv.ErrSyntax)

	// String
	field = fields[7]
	testCase(field, "string", nil)
	assertFieldValues(v.String, "string")

	// Bytes
	field = fields[8]
	testCase(field, "bytes", nil)
	assertFieldValues(v.Bytes, []byte("bytes"))

	// Slice
	field = fields[9]
	testCase(field, "1,2,3", nil)
	assertFieldValues(v.Slice, []int{1, 2, 3})
	testCase(field, "1,2bad", strconv.ErrSyntax)
	assertFieldValues(v.Slice, []int{1, 2, 3})

	// SliceStr
	field = fields[10]
	testCase(field, "text1,text2", nil)
	assertFieldValues(v.SliceStr, []string{"text1", "text2"})

	// Just for coverage not reached in real life
	brokenField := &model.Field{
		Value: reflect.Value{},
		Type:  reflect.TypeOf(make(chan struct{})),
	}

	testCase(brokenField, "1", util.ErrUnknownDestinationKind)
}
