package util

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitlab.com/so_literate/fconfig/internal/model"
)

// DurationType is a reflect type of the time.Duration.
var DurationType = reflect.TypeOf(time.Second)

// ErrUnknownDestinationKind returns when setter don't know how to set this value.
var ErrUnknownDestinationKind = errors.New("unknown destination kind")

func setFieldValueBool(dst reflect.Value, value string) error {
	v, err := strconv.ParseBool(value)
	if err != nil {
		return fmt.Errorf("strconv.ParseBool: %w", err)
	}

	dst.SetBool(v)
	return nil
}

func setFieldValueString(dst reflect.Value, value string) {
	dst.SetString(value)
}

func setFieldValueInt(dst reflect.Value, tp reflect.Type, value string) error {
	v, err := strconv.ParseInt(value, 0, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseInt: %w", err)
	}

	dst.SetInt(v)
	return nil
}

func setFieldValueInt64(dst reflect.Value, tp reflect.Type, value string) error {
	if tp == DurationType {
		v, err := time.ParseDuration(value)
		if err != nil {
			return fmt.Errorf("time.ParseDuration: %w", err)
		}

		dst.Set(reflect.ValueOf(v))
		return nil
	}

	return setFieldValueInt(dst, tp, value)
}

func setFieldValueUint(dst reflect.Value, tp reflect.Type, value string) error {
	v, err := strconv.ParseUint(value, 0, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseUint: %w", err)
	}

	dst.SetUint(v)
	return nil
}

func setFieldValueFloat(dst reflect.Value, tp reflect.Type, value string) error {
	v, err := strconv.ParseFloat(value, tp.Bits())
	if err != nil {
		return fmt.Errorf("strconv.ParseFloat: %w", err)
	}

	dst.SetFloat(v)
	return nil
}

func setFieldValueSlice(dst reflect.Value, tp reflect.Type, value string) error {
	// Slice of the uint8 it is a []byte
	if tp.Elem().Kind() == reflect.Uint8 {
		dst.Set(reflect.ValueOf([]byte(value)))
		return nil
	}

	values := strings.Split(value, ",")
	slice := reflect.MakeSlice(tp, len(values), len(values))

	for i, val := range values {
		err := setFieldValue(slice.Index(i), tp.Elem(), strings.TrimSpace(val))
		if err != nil {
			return fmt.Errorf("failed to set value into slice element [%d]: %w", i, err)
		}
	}

	dst.Set(slice)

	return nil
}

// setFieldValue sets value to dst of the field.
func setFieldValue(dst reflect.Value, tp reflect.Type, value string) error {
	if value == "" {
		return nil
	}

	kind := tp.Kind()

	// nolint:exhaustive // no need in every kinds here supported only.
	switch kind {
	case reflect.Bool:
		return setFieldValueBool(dst, value)

	case reflect.String:
		setFieldValueString(dst, value)
		return nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32:
		return setFieldValueInt(dst, tp, value)

	case reflect.Int64: // maybe time.Duration
		return setFieldValueInt64(dst, tp, value)

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return setFieldValueUint(dst, tp, value)

	case reflect.Float32, reflect.Float64:
		return setFieldValueFloat(dst, tp, value)

	case reflect.Slice:
		return setFieldValueSlice(dst, tp, value)
	}

	return fmt.Errorf("%w: kind %q (%v)", ErrUnknownDestinationKind, kind.String(), value)
}

// SetFieldValue sets value into field.
func SetFieldValue(field *model.Field, value string) error {
	err := setFieldValue(field.Value, field.Type, value)
	if err == nil {
		field.IsSet = true
	}

	return err
}
