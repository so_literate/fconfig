package util

import (
	"reflect"

	"gitlab.com/so_literate/fconfig/internal/model"
)

// IsAllocatedPointerToStruct returns true if value is pointer, not nil and struct.
func IsAllocatedPointerToStruct(value interface{}) bool {
	rv := reflect.ValueOf(value)

	if rv.Kind() != reflect.Ptr {
		return false
	}

	if rv.IsNil() {
		return false
	}

	if rv.Elem().Kind() != reflect.Struct {
		return false
	}

	return true
}

// AllocCopyValue returns pointer to the copy of value.
func AllocCopyValue(value interface{}) interface{} {
	return reflect.New(reflect.ValueOf(value).Elem().Type()).Interface()
}

// IsValueZero returns true when v has a zero value.
func IsValueZero(v reflect.Value) bool {
	zero := reflect.Zero(v.Type())
	return reflect.DeepEqual(v.Interface(), zero.Interface())
}

// MergeFields sets data from secondary fields to main.
func MergeFields(main, secondary []*model.Field, force bool) {
	for i := range main {
		if force || secondary[i].IsSet {
			main[i].Value.Set(secondary[i].Value)
			main[i].IsSet = true
		}
	}
}
