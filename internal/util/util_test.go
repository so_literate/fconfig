package util_test

import (
	"os"
	"testing"

	"gitlab.com/so_literate/fconfig/internal/util"
)

func TestGetEnv(t *testing.T) {
	t.Parallel()

	err := os.Setenv("UTIL_TEST_GO_GET_ENVS_METHOD", "=v=alu=e")
	if err != nil {
		t.Fatal(err)
	}

	env := util.GetEnv("UTIL_TEST_GO_")

	if len(env) != 1 {
		t.Fatal(env)
	}

	value := env["UTIL_TEST_GO_GET_ENVS_METHOD"]
	if value != "=v=alu=e" {
		t.Fatal(value)
	}

	err = os.Setenv("UTIL_TEST_GOLANG_GET_ENVS_METHOD", "value")
	if err != nil {
		t.Fatal(err)
	}

	env = util.GetEnv("") // empty prefix

	if len(env) < 2 {
		t.Fatal(env)
	}
}
