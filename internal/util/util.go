// Package util contain some helper methods to relieve main code.
package util

import (
	"os"
	"strings"
)

// GetEnv returns all environment variables from system with prefix.
func GetEnv(prefix string) map[string]string {
	env := os.Environ()

	res := make(map[string]string, len(env))

	for _, s := range env {
		if !strings.HasPrefix(s, prefix) {
			continue
		}

		for j := 0; j < len(s); j++ {
			if s[j] == '=' { // find first '=' symbol
				key, value := s[:j], s[j+1:]
				res[key] = value
				break
			}
		}
	}

	return res
}
