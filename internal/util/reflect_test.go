package util_test

import (
	"reflect"
	"testing"

	"gitlab.com/so_literate/fconfig/internal/analyzer"
	"gitlab.com/so_literate/fconfig/internal/util"
)

func TestIsAllocatedPointerToStruct(t *testing.T) {
	t.Parallel()

	testCase := func(value interface{}, want bool) {
		got := util.IsAllocatedPointerToStruct(value)
		if got != want {
			t.Fatalf("unexpected result: want %v, got %v\n\tvalue: %v (%T)", want, got, value, value)
		}
	}

	i := 0
	testCase(i, false)
	testCase(&i, false)

	type config struct {
		Field string
	}

	var value *config

	testCase(nil, false)
	testCase(config{}, false)
	testCase(value, false)

	value = &config{}
	testCase(value, true)
}

func TestAllocCopyValue(t *testing.T) {
	t.Parallel()

	type tp struct {
		F int
	}

	val := &tp{F: 1}
	cp := util.AllocCopyValue(val)

	cpVal, ok := cp.(*tp)
	if !ok {
		t.Fatalf("wrong type of the copy: %v (%T)", cp, cp)
	}

	if cpVal == val {
		t.Fatalf("copy have the same pointer: %v == %v", cpVal, val)
	}

	if cpVal == nil {
		t.Fatal("copy is nil")
	}

	if cpVal.F != 0 {
		t.Fatalf("copy have field value: %v", cpVal.F)
	}
}

func TestIsValueZero(t *testing.T) {
	t.Parallel()

	testCase := func(value reflect.Value, want bool) {
		got := util.IsValueZero(value)
		if want != got {
			t.Fatalf("unexpected result: want %v, got %v\n\tvalue: %v", want, got, value)
		}
	}

	var i int
	testCase(reflect.ValueOf(i), true)

	i = 1
	testCase(reflect.ValueOf(i), false)

	type st struct {
		field int
	}
	var s st
	testCase(reflect.ValueOf(s), true)

	s.field = 1
	testCase(reflect.ValueOf(s), false)

	type spt struct {
		pointer *st
	}
	var sp spt
	testCase(reflect.ValueOf(sp), true)

	sp.pointer = new(st)
	testCase(reflect.ValueOf(sp), false)

	testCase(reflect.ValueOf(sp.pointer), false)
	testCase(reflect.ValueOf(*sp.pointer), true)

	type sst struct {
		s st
	}
	var ss *sst
	testCase(reflect.ValueOf(ss), true)

	ss = new(sst)
	testCase(reflect.ValueOf(ss), false)
	testCase(reflect.ValueOf(*ss), true)
	testCase(reflect.ValueOf(ss.s), true)
	testCase(reflect.ValueOf(ss.s.field), true)

	ss.s.field = 1
	testCase(reflect.ValueOf(*ss), false)
	testCase(reflect.ValueOf(ss.s), false)
	testCase(reflect.ValueOf(ss.s.field), false)
}

func TestMergeFields(t *testing.T) {
	t.Parallel()

	type config struct {
		EmptyFirst  int
		EmptySecond int
	}

	mConf := &config{EmptySecond: 1}
	sConf := &config{EmptyFirst: 2}

	main := analyzer.New(false).GetFields(mConf)
	sec := analyzer.New(false).GetFields(sConf)

	if mConf.EmptyFirst != 0 {
		t.Fatalf("seted mConf.EmptyFirst: %v", mConf.EmptyFirst)
	}

	util.MergeFields(main, sec, false)

	if mConf.EmptyFirst != 2 {
		t.Fatalf("wrong value mConf.EmptyFirst: %v", mConf.EmptyFirst)
	}

	if mConf.EmptySecond != 1 {
		t.Fatalf("wrong value mConf.EmptySecond: %v", mConf.EmptySecond)
	}

	util.MergeFields(main, sec, true)

	if mConf.EmptyFirst != 2 {
		t.Fatalf("wrong value mConf.EmptyFirst: %v", mConf.EmptyFirst)
	}

	if mConf.EmptySecond != 0 {
		t.Fatalf("wrong value mConf.EmptySecond: %v", mConf.EmptySecond)
	}
}
