FConfig
========

Full config loader (default -> files -> env -> flags), lightweight and simple.

# Features

* Dependency-free, you don't need half a github to parse config
* Fully tested code
* Supports multiple sources of values
* * "Default" struct tag in code
* * Config files: .json, .env, .toml, .yaml, .hcl
* * Environment variables
* * Typed flags from command line
* Tags in struct: **default**, **usage**, **required**, **enum**, **flag**, **env** and others of file decoders (json, yaml, toml, hcl)
* Print default config into different sources (Flag help, Env, K8S values env, Files)

# Example

```go
package main

import (
	"fmt"

	"gitlab.com/so_literate/fconfig"
)

type Server struct {
	HTTPAddr string `default:":8080" usage:"address of the HTTP server"`
	Auth     struct {
		User string `required:"true" usage:"your user"`
		Pass string `required:"true" usage:"keep it in secret"`
	}
}

type SubConfig struct {
	Enum string `default:"value" enum:"value,other value" usage:"must be one of the list"`
}

type Embedded struct {
	EmbeddedField string `env:"FIELD" flag:"field" json:"field" usage:"field with special name"`
}

type Config struct {
	Server   *Server   // ENV: SERVER_HTTP_ADDR, SERVER_AUTH_USER. flags: server.http_addr, server.auth.user
	Sub      SubConfig // ENV: SUB_ENUM. flag: sub.enum
	Embedded           // ENV: FIELD. flag: field

	Values []int `enum:"[1 2],[3 4]" usage:"supported enums with slice too"`
	Empty  string
}

func main() {
	conf := new(Config)

	err := fconfig.New(conf).
		SkipFlags().                        // feel free to skip some steps.
		AddFile("./testdata/example.json"). // also you can add non-existent files (see FileNotFoundAlert).
		Load()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("HTTPAddr: %v\n", conf.Server.HTTPAddr) // conf.Server is not nil always.
	fmt.Printf("config: %#v\n", *conf)
}
```

See other examples in [pkg.go.dev](https://pkg.go.dev/gitlab.com/so_literate/fconfig)

# Additional information

## Supported types

FConfig supports basic types and slices.

Structs types:
- struct
- pointer to another struct
- embedded struct
- anonymous struct as field of struct

Field types:
- bool
- string
- int, int8, int16, int32, int64, time.Duration
- uint, uint8, uint16, uint32, uint64
- Slices of all types above ([]string, []int64). Doesn't support slice of structs.
- Pointers of all above types

## Struct tags

Documentation of the tags here: [pkg.go.dev](https://pkg.go.dev/gitlab.com/so_literate/fconfig#pkg-constants) 

```go
type Config struct {
	Field int `default:"1" enum:"1,2" required:"true" flag:"flag_name" env:"ENV_NAME" usage:"Flag help description"`
}
```

Tags `enum` and `required` do almost the same.

By default flag names generates into lower case with `.` as tree delimiter and `_` as camel case delimiter.
And generates environment variable name into UPPER case with `_` as tree delimiter and camel case delimiter.

The names for config files depend on the decoder rules, see below in **Config files**. 

```
type Config struct {
	Sub struct {
		CamelCaseField int
	}
}

flag name -> -sub.camel_case_field
env name  -> SUB_CAMEL_CASE_FIELD
```

You can set both delimiters (DelimiterEnv, DelimiterEnvWords, DelimiterFlag, DelimiterFlagWords)

Also you can set prefixes of the Envs and Flags (PrefixEnv, PrefixFlag)


## Config files

By default fconfig.Loader contains **JSON decoder only**.
If you want to decode other formats, you must add them to Loader.
Other decoders contain third-party dependencies.

```go
import "gitlab.com/so_literate/fconfig/decoder/yaml"

fconfig.New(new(Config)).AddFileDecoder(yaml.Decoder).AddFile("file.yaml").Load()
```

The loader **will continue to run** when it **cannot find the file**, 
if you want to stop then set FileNotFoundAlert parameter.

```go
fconfig.New(conf).FileNotFoundAlert()
```

All files **will merge values** from first to last if they are **not set** or **have not default value**.
Use a **pointer** if you want to definitely determine that value was set.
Or you can set true to **FileForceMerge** flag then you get all values from last file.

```go
--- stage.json
{
	"Int": 1,
	"String": "stage",
	"Bool": true
}

--- prod.json
{
	"String": "prod",
	"Bool": false
}

--- main.go
Config{
	Int: 1,         // From stage
	String: "prod", // Overwrite from prod
	Bool: true,     // because prod.json "bool" have default value.
}
```

File field names are regulated by tags and decoders rules. See documentation of the decoders:
- [dotenv](https://pkg.go.dev/gitlab.com/so_literate/fconfig#Config) - rules in the fconfig.Loader (PrefixEnv, DelimiterEnv, DelimiterEnvWords).
- [json](https://pkg.go.dev/encoding/json#Marshal) - Keep field name by default, use `json:""` tags.
- [hcl](https://github.com/hashicorp/hcl) - without `hcl:""` didn't work, also you need to set `,block` tag to structs.
- [toml](github.com/pelletier/go-toml) - similar to JSON with `toml:""` tags.
- [yaml](github.com/goccy/go-yaml) - by deault generates names in snake_case, also supports json tags.

### Printer

You can print default values into file or stdout in format of decoder or env, so you get example of the config.

```go
import "gitlab.com/so_literate/fconfig/decoder/json"

fconfig.New(new(Config)).PrintFileDefaults(json.Decoder)
```

See other examples in [pkg.go.dev](https://pkg.go.dev/gitlab.com/so_literate/fconfig)

# Ultimate Sources Example

```go
--- file.env
DOTENV="DOTENV"

--- file.hcl
HCL = "HCL"

--- file.json
{ "JSON": "JSON" }

--- file.toml
{ TOML = "TOML" }

--- file.yaml
yaml: YAML

--- main.go
package main

import (
	"fmt"
	"os"

	"gitlab.com/so_literate/fconfig"
	"gitlab.com/so_literate/fconfig/decoder/dotenv"
	"gitlab.com/so_literate/fconfig/decoder/hcl"
	"gitlab.com/so_literate/fconfig/decoder/toml"
	"gitlab.com/so_literate/fconfig/decoder/yaml"
)

type Config struct {
	Default string `default:"Default"`

	JSON   string
	DOTENV string
	YAML   string
	TOML   string
	HCL    string `hcl:"HCL"`

	EnvironmentVariable string
	Flag                string
}

func main() {
	conf := new(Config)

	loader := fconfig.New(conf).
		AddFile("file.json"). // json decoder already in loader.
		AddFileDecoder(dotenv.Decoder).AddFile("file.env").
		AddFileDecoder(hcl.Decoder).AddFile("file.hcl").
		AddFileDecoder(toml.Decoder).AddFile("file.toml").
		AddFileDecoder(yaml.Decoder).AddFile("file.yaml")

	os.Setenv("ENVIRONMENT_VARIABLE", "ENV") // Set ENV for yourself

	if err := loader.Load(); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("%#v\n", *conf)
}

```

Build into app and run:

```sh
app -flag=flag

main.Config{Default:"Default", JSON:"JSON", DOTENV:"DOTENV", YAML:"YAML", TOML:"TOML", HCL:"HCL", EnvironmentVariable:"ENV", Flag:"flag"}
```
